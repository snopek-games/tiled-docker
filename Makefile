build:
	docker build -t registry.gitlab.com/snopek-games/tiled-docker:local .

push:
	docker tag registry.gitlab.com/snopek-games/tiled-docker:local registry.gitlab.com/snopek-games/tiled-docker:latest
	docker push registry.gitlab.com/snopek-games/tiled-docker:latest

