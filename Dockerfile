FROM ubuntu:20.04

ENV TILED_VERSION 1.6.0

RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install --no-install-recommends -yq \
    apt-transport-https \
    ca-certificates \
	wget \
	build-essential \
	python3-dev \
	qt5-default \
	libqt5svg5 \
	qttools5-dev-tools \
	zlib1g-dev \
	qtdeclarative5-dev \
	qtdeclarative5-private-dev \
	qbs \
	xvfb \
 && rm -rf /var/lib/apt/lists/*

RUN mkdir -p /src \
 && cd /src \
 && wget https://github.com/mapeditor/tiled/archive/refs/tags/v$TILED_VERSION.tar.gz -O tiled.tar.gz \
 && tar -xzvf tiled.tar.gz \
 && rm -f tiled.tar.gz \
 && cd tiled-$TILED_VERSION \
 && qbs setup-toolchains --detect \
 && qbs setup-qt --detect \
 && qbs install --install-root / qbs.installPrefix:/usr projects.Tiled.useRPaths:false \
 && qbs clean

